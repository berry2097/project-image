USE [db_Mahasiswa]
GO
/****** Object:  Table [dbo].[DataMahasiswa]    Script Date: 19/07/2022 11:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataMahasiswa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nim] [varchar](10) NULL,
	[nama] [varchar](50) NULL,
	[jurusan] [varchar](50) NULL,
	[ImagePath] [varchar](max) NULL,
 CONSTRAINT [PK_DataMahasiswa] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Image]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Image](
	[ImageID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](250) NULL,
	[ImagePath] [varchar](max) NULL,
 CONSTRAINT [PK_Image] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](60) NULL,
	[Username] [varchar](50) NULL,
	[RoleCode] [varchar](20) NULL,
	[Gender] [varchar](50) NULL,
	[DateOfBirth] [date] NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
 CONSTRAINT [PK_Uaer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DataMahasiswa] ON 

INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (4, N'001', N'Berry Juanda ', N'Sistem Informasi ', N'~/Content/img/images220347939.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (5, N'002', N'Riska Inawati', N'Akuntansi', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (11, N'003', N'Fahrul Malik', N'Teknik Komputer', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (12, N'004', N'Yeti Nuraeni', N'Desain Komunikasi Visual', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (13, N'005', N'Tama Syahrudin', N'Teknik Industri', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (14, N'006', N'Misca Khoirul Fatwa', N'Komputerisasi Akuntansi', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (15, N'007', N'Rois Fathul Hudaya', N'Teknik Informatika', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (16, N'008', N'Michelle Hwang', N'Hukum', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (17, N'009', N'Yonathan ', N'Sastra Inggris', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (18, N'010', N'Fahmi Qadri', N'Teknik Sipil', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (19, N'011', N'Fatwa', N'Sastra Prancis', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (20, N'012', N'Yuliani', N'Ilmu Komunikasi', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (21, N'013', N'Rina Nose', N'Sastra Jepang', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (22, N'014', N'Risma', N'Hubungan Internasional', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (23, N'015', N'Ridwan', N'Ilmu Ekonomi', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (24, N'016', N'Sonny', N'Teknik Industri', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (25, N'017', N'Carmila', N'Sastra Sunda', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (26, N'018', N'Kuntoro', N'Farmasi', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (27, N'019', N'Selly Tri Wardani Setyaningsih', N'Seni Budaya', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (28, N'020', N'Datuk Khalifah', N'Perhotelan', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (29, N'021', N'Marsha', N'Akuntansi Manajemen Pemerintahan', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[DataMahasiswa] ([id], [nim], [nama], [jurusan], [ImagePath]) VALUES (30, N'022', N'Bintang', N'Teknik Komputer', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
SET IDENTITY_INSERT [dbo].[DataMahasiswa] OFF
GO
SET IDENTITY_INSERT [dbo].[Image] ON 

INSERT [dbo].[Image] ([ImageID], [Title], [ImagePath]) VALUES (5, N'Gelas', N'~/Content/img/Gelas Karakter220716519.jpg')
INSERT [dbo].[Image] ([ImageID], [Title], [ImagePath]) VALUES (6, N'Paris', N'~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg')
INSERT [dbo].[Image] ([ImageID], [Title], [ImagePath]) VALUES (7, N'Kubah', N'~/Content/img/back8225041962.jpg')
SET IDENTITY_INSERT [dbo].[Image] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (6, N'admin', N'admin', N'admin', N'M', CAST(N'2022-07-18' AS Date), N'admin_mahasiswa@gmail.com', N'admin')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (17, N'Susi', N'susi', N'user', N'F', CAST(N'1997-12-24' AS Date), N'susi@gmail.com', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (18, N'Latifah', N'latifah', N'user', N'F', CAST(N'2022-03-15' AS Date), N'latifah@gmail.com', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (19, N'Berry Juanda Prawira', N'berry', N'user', N'M', CAST(N'2021-07-23' AS Date), N'berryjuanda@gmail.com', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (20, N'Renaldy ', N'renaldy', N'user', N'M', CAST(N'2022-07-06' AS Date), N'renaldy@gmail.com', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (21, N'Fikri', N'fikri', N'user', N'M', CAST(N'2022-07-12' AS Date), N'fikri@gmail', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (22, N'Frida', N'frida', N'user', N'F', CAST(N'1996-05-01' AS Date), N'frida@gmail.com', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (23, N'Nandar', N'nandar20', N'user', N'M', CAST(N'2022-07-25' AS Date), N'nandar@gmail.com', N'123')
INSERT [dbo].[User] ([id], [Name], [Username], [RoleCode], [Gender], [DateOfBirth], [Email], [Password]) VALUES (24, N'Ammar', N'amar', N'user', N'M', CAST(N'2022-07-01' AS Date), N'ammar@gamil.com', N'123')
SET IDENTITY_INSERT [dbo].[User] OFF
GO
/****** Object:  StoredProcedure [dbo].[stp_AuthRegister]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_AuthRegister]
	-- Add the parameters for the stored procedure here
	@name varchar(60),
	@username varchar(50),
	@role varchar(20),
	@gender varchar(50),
	@birth DATE,
	@email varchar(50),
	@password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [db_Mahasiswa].[dbo].[User](Name,Username,RoleCode,Gender,DateOfBirth,Email,Password) VALUES(@name,@username,@role,@gender,@birth,@email,@password)
END

GO
/****** Object:  StoredProcedure [dbo].[stp_DeleteImage]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_DeleteImage] 
	-- Add the parameters for the stored procedure here
	@id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM Image WHERE ImageID=@id 
END
GO
/****** Object:  StoredProcedure [dbo].[stp_DeleteMahasiswa]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_DeleteMahasiswa] 
	-- Add the parameters for the stored procedure here
	@nim varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM DataMahasiswa WHERE nim=@nim
END

--execute stp_DeleteMahasiswa '023';
--execute stp_GetMahasiswa;
GO
/****** Object:  StoredProcedure [dbo].[stp_DetailImage]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_DetailImage]
	-- Add the parameters for the stored procedure here
	@id int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Image WHERE ImageID=@id
END

--execute stp_DetailImage 2;
GO
/****** Object:  StoredProcedure [dbo].[stp_DisplayImage]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_DisplayImage] 
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM Image
END
--execute stp_DisplayImage;
GO
/****** Object:  StoredProcedure [dbo].[stp_GetAdmin]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_GetAdmin] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [db_Mahasiswa].[dbo].[User] WHERE RoleCode = 'admin'
END
GO
/****** Object:  StoredProcedure [dbo].[stp_GetAllUser]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_GetAllUser]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [db_Mahasiswa].[dbo].[User]
END
GO
/****** Object:  StoredProcedure [dbo].[stp_GetMahasiswa]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_GetMahasiswa] 
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [db_Mahasiswa].[dbo].[DataMahasiswa]
END

--execute stp_GetMahasiswa;
GO
/****** Object:  StoredProcedure [dbo].[stp_getNim]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_getNim]
	-- Add the parameters for the stored procedure here
		@nim varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM DataMahasiswa Where nim=@nim
END

--execute stp_getNim '002';
--execute stp_GetMahasiswa;
GO
/****** Object:  StoredProcedure [dbo].[stp_GetUsername]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_GetUsername]
	-- Add the parameters for the stored procedure here
	@username varchar(50),
	@password varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [db_Mahasiswa].[dbo].[User] WHERE Username=@username AND Password=@password
END

--execute stp_GetUsername 'berry','123';
GO
/****** Object:  StoredProcedure [dbo].[stp_InsertMahasiswa]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_InsertMahasiswa] 
	-- Add the parameters for the stored procedure here
	@nim varchar(10),
	@nama varchar(50),
	@jurusan varchar(50),
	@imagePath varchar (MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [db_Mahasiswa].[dbo].[DataMahasiswa](nim,nama,jurusan,ImagePath) VALUES(@nim,@nama,@jurusan,@imagePath)
END

--execute stp_InsertMahasiswa '022','Bintang','Teknik Komputer','~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg';
--execute stp_GetMahasiswa;
GO
/****** Object:  StoredProcedure [dbo].[stp_PageMahasiswa]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_PageMahasiswa] 
	-- Add the parameters for the stored procedure here
	@skip int,
	@take int
AS
BEGIN
	  
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
FROM DataMahasiswa
ORDER BY nim
OFFSET @skip ROWS FETCH NEXT @take ROWS ONLY
END

--execute stp_PageMahasiswa 0,7;



GO
/****** Object:  StoredProcedure [dbo].[stp_UpdateImage]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_UpdateImage] 
	-- Add the parameters for the stored procedure here
	@id int,
	@title varchar(250),
	@path varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [db_Mahasiswa].[dbo].[Image] set Title=@title, ImagePath=@path 
	WHERE ImageID=@id
END

--execute stp_UpdateImage @id=2,@title='Kapak',@path='~/Content/img/autumn-sunrise-eiffel-tower-paris-france-wallpaper-preview221210884.jpg';
GO
/****** Object:  StoredProcedure [dbo].[stp_UpdateMahasiswa]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_UpdateMahasiswa] 
	-- Add the parameters for the stored procedure here
	@nim varchar(10),
	@nama varchar(50),
	@jurusan varchar(50),
	@imagePath varchar (MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE DataMahasiswa set nama=@nama, jurusan=@jurusan, ImagePath=@imagePath
	WHERE nim=@nim
END

--execute [dbo].[stp_UpdateMahasiswa] '004','Doni edit','Sistem informasi';
--execute stp_GetMahasiswa;
GO
/****** Object:  StoredProcedure [dbo].[stp_UploadImage]    Script Date: 19/07/2022 11:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[stp_UploadImage] 
	-- Add the parameters for the stored procedure here
	@title varchar(250),
	@imagePath varchar (MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [db_Mahasiswa].[dbo].[Image](Title,ImagePath) VALUES (@title,@imagePath) 
END



GO
