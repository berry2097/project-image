﻿using Mahasiswa.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Mahasiswa.Services
{
    public class EmployeeServices
    {
        string mainConnection = ConfigurationManager.ConnectionStrings["MahasiswaConnection"].ConnectionString;
        public List<Employee> ListAll()
        {
            List<Employee> lst = new List<Employee>();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_GetEmployee", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Employee emp = new Employee();
                    emp.EmployeeID = Convert.ToInt32(dr["EmployeeID"]);
                    emp.Name = dr["Name"].ToString();
                    emp.Age = Convert.ToInt32(dr["Age"]);
                    emp.State = dr["State"].ToString();
                    emp.Country = dr["Country"].ToString();
                    lst.Add(emp);
                }
            }
            conn.Close();
            return lst;
        }

        public bool Add(Employee emp)
        {
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_InsertEmployee", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@name", emp.Name);
            cmd.Parameters.AddWithValue("@age", Convert.ToInt32(emp.Age));
            cmd.Parameters.AddWithValue("@state", emp.State);
            cmd.Parameters.AddWithValue("@Country", emp.Country);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            var is_success = false;
            if(dr.RecordsAffected == 1)
            {
                is_success = true;
            }
            return is_success;
        }

        public Employee getDetail(int id)
        {
            Employee emp = new Employee();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_DetailEmployee", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    emp.EmployeeID = Convert.ToInt32(dr["EmployeeID"]);
                    emp.Name = dr["Name"].ToString();
                    emp.Age = Convert.ToInt32(dr["Age"]);
                    emp.State = dr["State"].ToString();
                    emp.Country = dr["Country"].ToString();
                }
            }
            conn.Close();
            return emp;
        }

        public List<Employee> Edit(Employee emp)
        {
            List<Employee> lst = new List<Employee> ();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_UpdateEmployee", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id",Convert.ToInt32(emp.EmployeeID));
            cmd.Parameters.AddWithValue("@name", emp.Name);
            cmd.Parameters.AddWithValue("@Age", Convert.ToInt32(emp.Age));
            cmd.Parameters.AddWithValue("@state", emp.State);
            cmd.Parameters.AddWithValue("@country", emp.Country);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Employee edit = new Employee();
                    edit.EmployeeID = Convert.ToInt32(dr["EmployeeID"]);
                    edit.Name = dr["Name"].ToString();
                    edit.Age = Convert.ToInt32(dr["Age"]);
                    edit.State = dr["State"].ToString();
                    edit.Country = dr["Country"].ToString();
                    lst.Add(edit);
                }
            }
            conn.Close();
            return lst;
        }

        public List<Employee> Delete(int id)
        {
            List<Employee> lst = new List<Employee> ();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_DeleteEmployee", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id", id);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Employee employee = new Employee();
                    employee.EmployeeID = Convert.ToInt32(dr["EmployeeID"]);
                    employee.Name = dr["Name"].ToString();
                    employee.Age = Convert.ToInt32(dr["Age"]);
                    employee.State = dr["State"].ToString();
                    employee.Country = dr["Country"].ToString();
                    lst.Add(employee);
                }
            }
            conn.Close();
            return lst;
        }
    }
}