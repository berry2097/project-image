﻿using Mahasiswa.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Mahasiswa.Services
{
    public class UserServices
    {
        string mainConnection = WebConfigurationManager.ConnectionStrings["MahasiswaConnection"].ConnectionString;
        public bool register(UserModel user)
        {
            List<UserModel> userList = new List<UserModel>();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_AuthRegister", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@name", user.Name);
            cmd.Parameters.AddWithValue("@username", user.Username);
            cmd.Parameters.AddWithValue("@role", user.RoleCode);
            cmd.Parameters.AddWithValue("@gender", user.Gender);
            cmd.Parameters.AddWithValue("@birth", Convert.ToDateTime(user.DateOfBirth));
            cmd.Parameters.AddWithValue("@email", user.Email);
            cmd.Parameters.AddWithValue("@password", user.Password);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            var is_success = false;
            if(dr.RecordsAffected == 1)
            {
                is_success = true;
            }
            return is_success;
        }

        public UserModel getAdmin()
        {
            UserModel getAdmin = new UserModel();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_GetAdmin", conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    getAdmin.id = Convert.ToInt32(dr["id"]);
                    getAdmin.Name = dr["Name"].ToString();
                    getAdmin.Username = dr["Username"].ToString();
                    getAdmin.RoleCode = dr["RoleCode"].ToString();
                    getAdmin.Gender = dr["Gender"].ToString();
                    getAdmin.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"]);
                    getAdmin.Email = dr["Email"].ToString();
                    getAdmin.Password = dr["Password"].ToString();
                }
            }
            conn.Close();
            return getAdmin;
        }

      
      public UserModel getUser(string Username, string Password)
        {
            UserModel user = new UserModel();
            SqlConnection conn = new SqlConnection(mainConnection);
            SqlCommand cmd = new SqlCommand("stp_GetUsername",conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@username", Username);
            cmd.Parameters.AddWithValue("@password", Password);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    user.id = Convert.ToInt32(dr["id"]);
                    user.Name = dr["Name"].ToString();
                    user.Username = dr["Username"].ToString();
                    user.RoleCode = dr["RoleCode"].ToString();
                    user.Gender = dr["Gender"].ToString();
                    user.DateOfBirth = Convert.ToDateTime(dr["DateOfBirth"]);
                    user.Email = dr["Email"].ToString();
                    user.Password = dr["Password"].ToString();
                }
            }
            return user;
        }
    }
}