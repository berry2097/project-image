﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Mahasiswa.Models
{
    public class AplicationModels
    {

    }

    public class MahasiswaModel
    {
        // public string id { get; set; }
        [Required(ErrorMessage = "Please enter nim!")]
        [StringLength(10, MinimumLength = 3)]
        public string nim { get; set; }
        [Required(ErrorMessage = "Please enter nama!")]
        public string nama { get; set; }
        [Required(ErrorMessage = "Please eneter jurusan!")]
        public string jurusan { get; set; }
        [DisplayName("Upload File")]
        public string ImagePath { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }

        public HttpPostedFileBase ImageFileEdit { get; set; }
    }

    public class UserModel
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Please Enter Name!")]
        public string Name{ get; set; }
        [Required(ErrorMessage ="Please Enter Username!")]
        public string Username { get; set; }
        public string RoleCode { get; set; }
        [Required(ErrorMessage = "Please Enter Gender!")]
       // [EnumDataType(typeof(UserGender))]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Please Enter DateOfBirth!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage ="Please Enter Email!")]
        public string Email { get; set; }
        [Required(ErrorMessage="Please Eneter Password!")]
        public string Password { get; set; }
    }
}