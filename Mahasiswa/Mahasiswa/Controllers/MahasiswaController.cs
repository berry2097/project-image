﻿using Mahasiswa.Models;
using Mahasiswa.Services;
using System;
using System.IO;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Mahasiswa.Controllers
{

    public class MahasiswaController : Controller
    {
        //string mainConection = ConfigurationManager.AppSettings["MahasiswaConnection"];
        string mainConnection = WebConfigurationManager.ConnectionStrings["MahasiswaConnection"].ConnectionString;
        MahasiswaServices mahasiswaservices = new MahasiswaServices();
        UserServices userservices = new UserServices();
        // GET: Mahasiswa
        public ActionResult Index(int pg = 1, int skip = 0, int take = 10, string search = "")
        {

            // ViewBag.mahasiswa = mahasiswaservices.mahasiswaData();

            const int pageSize = 10;
            if (pg < 1)
                pg = 1;

            skip = (pg - 1) * pageSize;
            take = pageSize;
            //int record = 1;
            ViewBag.pageMahasiswa = mahasiswaservices.pageMahasiswa(skip, take);
            //var result = ViewBag.pageMahasiswa.Skip(skip).Take(take).ToList();
            int recsCount = mahasiswaservices.getJmlMahasiswa();
            var pager = new Pager(recsCount, pg, pageSize);
            ViewBag.search = mahasiswaservices.searchMahasiswa(search);
            ViewBag.Pager = pager;

            return View();

        }




        // GET : Mahasiswa/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST : Mahasiswa/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MahasiswaModel mahasiswa)
        {
            // Get Image Path
            string fileName = Path.GetFileNameWithoutExtension(mahasiswa.ImageFile.FileName);
            string extension = Path.GetExtension(mahasiswa.ImageFile.FileName);
            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
            mahasiswa.ImagePath = "~/Content/img/" + fileName;
            fileName = Path.Combine(Server.MapPath("~/Content/img/" + fileName));
            mahasiswa.ImageFile.SaveAs(fileName);
            // Get Image Path /

            ViewBag.mahasiswa = mahasiswaservices.insertMahasiswa(mahasiswa);
            ModelState.Clear();
            return RedirectToAction("index");
        }

        // GET : Mahasiswa/Edit
        [HttpGet]
        public ActionResult Edit(string nim)
        {
            ViewBag.edit = mahasiswaservices.getDetails(nim);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MahasiswaModel mahasiswa)
        {
            string fileEditName = Path.GetFileNameWithoutExtension(mahasiswa.ImageFileEdit.FileName);
            string extension = Path.GetExtension(mahasiswa.ImageFileEdit.FileName);
            fileEditName = fileEditName + DateTime.Now.ToString("yymmssfff") + extension;
            mahasiswa.ImagePath = "~/Content/img/" + fileEditName;
            fileEditName = Path.Combine(Server.MapPath("~/Content/img/" + fileEditName));
            mahasiswa.ImageFileEdit.SaveAs(fileEditName);

            ViewBag.mahasiswa = mahasiswaservices.editMahasiswa(mahasiswa);
            ModelState.Clear();
            return RedirectToAction("index");

        }

        [HttpGet]
        public ActionResult Details(string nim)
        {
            ViewBag.details = mahasiswaservices.getDetails(nim);
            ViewBag.nim = ViewBag.details.nim;
            ViewBag.nama = ViewBag.details.nama;
            ViewBag.jurusan = ViewBag.details.jurusan;
            ViewBag.path = ViewBag.details.ImagePath;
            return View();
        }

        [HttpGet]
        public ActionResult Delete(string nim)
        {
            ViewBag.delete = mahasiswaservices.getDetails(nim);
            ViewBag.nim = ViewBag.delete.nim;
            ViewBag.nama = ViewBag.delete.nama;
            ViewBag.jurusan = ViewBag.delete.jurusan;
            ViewBag.path = ViewBag.delete.ImagePath;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(MahasiswaModel mahasiswa, string nim)
        {
            ViewBag.delete = mahasiswaservices.deleteMahasiswa(mahasiswa, nim);
            return RedirectToAction("index");
        }


        // Halaman Register
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserModel user)
        {
            ViewBag.register = userservices.register(user);
            return RedirectToAction("Login");
        }

        // Form Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserModel user, string Username, string Password)
        {
            ViewBag.message = "";
            if (Username == "admin" && Password == "admin")
            {
                ViewBag.admin = userservices.getAdmin();
                Session["RoleCode"] = ViewBag.admin.RoleCode;
                Session["Name"] = ViewBag.admin.Name;
                Session["Username"] = ViewBag.admin.Username;
                Session["Gender"] = ViewBag.admin.Gender;
                Session["DateOfBirth"] = ViewBag.admin.DateOfBirth;
                Session["Email"] = ViewBag.admin.Email;
                return RedirectToAction("Index_admin", "Home");
            }
            else
            {
                ViewBag.message = "Please check your username & password!";
            }
            if (Username.Length < 3 || Username == null)
            {
                ViewBag.message = "Please check your username & password";
            }
            else
            {

                ViewBag.getuser = userservices.getUser(Username, Password);
                if (Username == ViewBag.getuser.Username && Password == ViewBag.getuser.Password)
                {
                    var date = ViewBag.getuser.DateOfBirth.ToString("MM/dd/yyyy");
                    //var date = DateTime.ParseExact(ViewBag.getuser.DateOfBirth, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    Session["RoleCode"] = ViewBag.getuser.RoleCode;
                    Session["Name"] = ViewBag.getuser.Name;
                    Session["Username"] = ViewBag.getuser.Username;
                    Session["Gender"] = ViewBag.getuser.Gender;
                    Session["DateOfBirth"] = date;
                    Session["Email"] = ViewBag.getuser.Email;
                    return RedirectToAction("Index_user", "Home");
                }
                else
                {
                    ViewBag.message = "Please Check your username & password!";
                }


            }
            return View();
        }

        public ActionResult Logout()
        {
            Session["RoleCode"] = "";
            Session["Name"] = "";
            Session["Username"] = "";
            Session["Gender"] = "";
            Session["DateOfBirth"] = "";
            Session["Email"] = "";

            return RedirectToAction("Login", "Mahasiswa");
        }
    }
}