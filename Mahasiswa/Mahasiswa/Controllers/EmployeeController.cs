﻿using Mahasiswa.Models;
using Mahasiswa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Mahasiswa.Controllers
{

    public class EmployeeController : Controller
    {
        EmployeeServices employeeservices = new EmployeeServices();
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult ViewListModal()
        {
                ViewBag.list = employeeservices.ListAll();
                var data = ViewBag.list;
                return Json(data, JsonRequestBehavior.AllowGet);
            
        }

        [HttpPost]
        public JsonResult CreateViaModal(Employee emp)
        {
            ViewBag.data = employeeservices.Add(emp);
            return Json(new { ErrorMessage = "success" });
        }

        public JsonResult Detail(int id)
        {
            ViewBag.detail = employeeservices.getDetail(id);
            var detail = ViewBag.detail;
            return Json(detail, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Edit(Employee emp)
        {
            ViewBag.getId = employeeservices.Edit(emp);
            var edit = ViewBag.getId;
            return Json(edit, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            ViewBag.delete = employeeservices.Delete(id);
            var delete = ViewBag.delete;
            return Json(delete,JsonRequestBehavior.AllowGet);
        }
    }
}